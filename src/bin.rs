// use advent_2020::day_01;
// use advent_2020::day_02;
use advent_2020::day_03;

use std::io;

pub fn main() {
    let stdin = io::stdin();
    let input = stdin.lock();
    let output = io::stdout();

    // day_01::p1::solve(input, output);
    // day_01::p2::solve(input, output);
    // day_02::p1::solve(input, output);
    // day_02::p2::solve(input, output);
    // day_03::p1::solve(input, output);
    day_03::p2::solve(input, output);
}
