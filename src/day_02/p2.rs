/*
--- Part Two ---
While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy enforcement.

Given the same example list from above:

1-3 a: abcde is valid: position 1 contains a and position 3 does not.
1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
How many passwords are valid according to the new interpretation of the policies?
*/

use regex::Regex;
use std::io::{BufRead, Write};

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let input_lines = parse_input(reader);
    let mut res = 0;
    for l in input_lines {
        if is_valid_pw(&l.pw, &l.policy) {
            res += 1;
        }
    }
    write!(&mut writer, "The number of valid passwords is: {}", res).unwrap();
}

fn parse_input<R>(reader: R) -> Vec<InputLine> where R: BufRead {
    let mut res = Vec::new();
    for line in reader.lines() {
        res.push(parse_line(&line.unwrap()));
    }
    res
}

fn parse_line(input_line: &str) -> InputLine {
    let regex = Regex::new(r"(\d+)-(\d+) (\w): (\w+)").unwrap();
    let captures = regex.captures(input_line).unwrap();
    InputLine {
        policy: PwPolicy {
            first_position: captures[1].parse::<usize>().unwrap(),
            last_position: captures[2].parse::<usize>().unwrap(),
            character: captures[3].chars().nth(0).unwrap()
        },
        pw: captures[4].to_string()
    }
}

#[derive(PartialEq, Debug)]
struct InputLine {
    pw: String,
    policy: PwPolicy
}

#[derive(PartialEq, Debug)]
struct PwPolicy {
    first_position: usize,
    last_position: usize,
    character: char
}

fn is_valid_pw(pw: &str, policy: &PwPolicy) -> bool {
    let mut occurrences = 0;
    let char_first_pos = pw.chars().nth(policy.first_position - 1).unwrap();
    let char_last_pos = pw.chars().nth(policy.last_position - 1).unwrap();
    if char_first_pos == policy.character {
        occurrences += 1;
    }
    if char_last_pos == policy.character {
        occurrences += 1;
    }
    occurrences == 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_pw() {
        let pw = String::from("abcde");
        let policy = PwPolicy {
            first_position: 1,
            last_position: 3,
            character: 'a'
        };
        assert!(is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_invalid_pw_where_neither_position_contains_the_character() {
        let pw = String::from("cdefg");
        let policy = PwPolicy {
            first_position: 1,
            last_position: 3,
            character: 'b'
        };
        assert!(!is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_invalid_pw_where_both_positions_contain_the_character() {
        let pw = String::from("ccccccccc");
        let policy = PwPolicy {
            first_position: 2,
            last_position: 9,
            character: 'c'
        };
        assert!(!is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_solve() {
        let input_str = b"1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
        let mut output = Vec::new();
        let expected = String::from("The number of valid passwords is: 1");

        solve(&input_str[..], &mut output);
        assert_eq!(String::from_utf8(output).unwrap(), expected);
    }

}
