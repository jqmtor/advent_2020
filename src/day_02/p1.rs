/*
--- Day 2: Password Philosophy ---
Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.

Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.

How many passwords are valid according to their policies?
 */

use regex::Regex;
use std::io::{BufRead, Write};

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let input_lines = parse_input(reader);
    let mut res = 0;
    for l in input_lines {
        if is_valid_pw(&l.pw, &l.policy) {
            res += 1;
        }
    }
    write!(&mut writer, "The number of valid passwords is: {}", res).unwrap();
}

fn parse_input<R>(reader: R) -> Vec<InputLine> where R: BufRead {
    let mut res = Vec::new();
    for line in reader.lines() {
        res.push(parse_line(&line.unwrap()));
    }
    res
}

fn parse_line(input_line: &str) -> InputLine {
    let regex = Regex::new(r"(\d+)-(\d+) (\w): (\w+)").unwrap();
    let captures = regex.captures(input_line).unwrap();
    InputLine {
        policy: PwPolicy {
            min: captures[1].parse::<u32>().unwrap(),
            max: captures[2].parse::<u32>().unwrap(),
            character: captures[3].chars().nth(0).unwrap()
        },
        pw: captures[4].to_string()
    }
}

#[derive(PartialEq, Debug)]
struct InputLine {
    pw: String,
    policy: PwPolicy
}

#[derive(PartialEq, Debug)]
struct PwPolicy {
    min: u32,
    max: u32,
    character: char
}

fn is_valid_pw(pw: &str, policy: &PwPolicy) -> bool {
    let mut count = 0;
    for c in pw.chars() {
        if c == policy.character {
            count += 1;
        }
    }
    count >= policy.min && count <= policy.max
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_input_first_line() {
        // 1-3 a: abcde
        let policy = PwPolicy {
            min: 1,
            max: 3,
            character: 'a'
        };
        let pw = "abcde";

        assert!(is_valid_pw(pw, &policy));
    }

    #[test]
    fn test_example_input_second_line() {
        // 1-3 b: cdefg
        let policy = PwPolicy {
            min: 1,
            max: 3,
            character: 'b'
        };
        let pw = "cdefg";

        assert!(!is_valid_pw(pw, &policy));
    }

    #[test]
    fn test_parse_line() {
        let str_input = "1-3 a: abcde";
        let parsed = parse_line(str_input);
        let expected = InputLine {
            pw: String::from("abcde"),
            policy: PwPolicy {
                min: 1,
                max: 3,
                character: 'a'
            }
        };
        assert_eq!(parsed, expected);
    }

    #[test]
    fn test_parse_multi_line_input() {
        let str_input = b"1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
        let input = parse_input(&str_input[..]);
        let expected = vec![
            InputLine {
                pw: String::from("abcde"),
                policy: PwPolicy {
                    min: 1,
                    max: 3,
                    character: 'a'
                }
            },
            InputLine {
                pw: String::from("cdefg"),
                policy: PwPolicy {
                    min: 1,
                    max: 3,
                    character: 'b'
                }
            },
            InputLine {
                pw: String::from("ccccccccc"),
                policy: PwPolicy {
                    min: 2,
                    max: 9,
                    character: 'c'
                }
            }
        ];
        assert_eq!(input, expected);
    }

    #[test]
    fn test_invalid_pw_below_limit() {
        let pw = String::from("abcde");
        let policy = PwPolicy {
            min: 2,
            max: 3,
            character: 'a'
        };
        assert!(!is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_invalid_pw_above_limit() {
        let pw = String::from("aaaae");
        let policy = PwPolicy {
            min: 1,
            max: 3,
            character: 'a'
        };
        assert!(!is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_valid_pw() {
        let pw = String::from("abc");
        let policy = PwPolicy {
            min: 1,
            max: 3,
            character: 'a'
        };
        assert!(is_valid_pw(&pw, &policy));
    }

    #[test]
    fn test_solve() {
        let input_str = b"1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
        let mut output = Vec::new();
        let expected = String::from("The number of valid passwords is: 2");

        solve(&input_str[..], &mut output);
        assert_eq!(String::from_utf8(output).unwrap(), expected);
    }

}
