/*
--- Day 1: Report Repair ---
After saving Christmas five years in a row, you've decided to take a vacation at a nice resort on a tropical island. Surely, Christmas will go on without you.

The tropical island has its own currency and is entirely cash-only. The gold coins used there have a little picture of a starfish; the locals just call them stars. None of the currency exchanges seem to have heard of them, but somehow, you'll need to find fifty of these coins by the time you arrive so you can pay the deposit on your room.

To save your vacation, you need to get all fifty stars by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.

Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.

For example, suppose your expense report contained the following:

1721
979
366
299
675
1456
In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.

Of course, your expense report is much larger. Find the two entries that sum to 2020; what do you get if you multiply them together?
*/

use std::io::{BufRead, Write};
use std::collections::HashMap;

// This is how the solution would be called if this had an entry point.
// fn main() {
//     let stdin = io::stdin();
//     let input = stdin.lock();
//     let output = io::stdout();
//     solve(input, output);
// }

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let solution = parse_input(reader)
        .and_then(|coll| find_pair(&coll))
        .and_then(|(val_1, val_2)| Ok(val_1 * val_2))
        .expect("Failure");

    write!(&mut writer, "The resulting value is: {}", solution).unwrap();
}

fn parse_input<R>(reader: R) -> Result<Vec<i32>, String> where R: BufRead {
    let mut coll = Vec::new();
    let mut num: i32;
    for line in reader.lines() {
        if let Ok(num_str) = line {
            num = num_str.parse::<i32>().map_err(|_| String::from("Parse error"))?;
            coll.push(num);
        }
    }
    Ok(coll)
}

// TODO: build this index incrementally to solve the problem in one pass of the array
fn build_index(coll: &Vec<i32>) -> HashMap<i32, usize> {
    let mut index = HashMap::new();
    for (i, elem) in coll.iter().enumerate() {
        index.insert(2020 - elem, i);
    }
    index
}

fn find_pair(coll: &Vec<i32>) -> Result<(i32, i32), String> {
    let index = build_index(&coll);
    for x in coll {
        if let Some(complement_idx) = index.get(&x) {
            return Ok((*x, coll[*complement_idx]))
        }
    }
    Err("No valid pair was found".to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_pair_sample_input() {
        let coll = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_pair(&coll).unwrap();
        assert_eq!(result, (1721, 299));
    }

    #[test]
    fn test_parse_input() {
        let input_str = b"1721\n979\n366\n299\n675\n1456";

        let expected = vec![1721, 979, 366, 299, 675, 1456];
        assert_eq!(parse_input(&input_str[..]).unwrap(), expected);
    }

    #[test]
    fn test_solve() {
        let input_str = b"1721\n979\n366\n299\n675\n1456";
        let mut output = Vec::new();
        let expected = String::from("The resulting value is: 514579");

        solve(&input_str[..], &mut output);

        assert_eq!(String::from_utf8(output).unwrap(), expected);
    }
}
