/*
--- Part Two ---
The Elves in accounting are thankful for your help; one of them even offers you a starfish coin they had left over from a past vacation. They offer you a second one if you can find three numbers in your expense report that meet the same criteria.

Using the above example again, the three entries that sum to 2020 are 979, 366, and 675. Multiplying them together produces the answer, 241861950.

In your expense report, what is the product of the three entries that sum to 2020?
*/

use std::io::{BufRead, Write};

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let result = parse_input(reader)
        .and_then(|coll| find_triplet(&coll))
        .and_then(|(val_1, val_2, val_3)| Ok(val_1 * val_2 * val_3));

    match result {
        Ok(r) => write!(&mut writer, "The resulting value is: {}", r).unwrap(),
        Err(msg) => write!(&mut writer, "{}", msg).unwrap()
    }
}

fn parse_input<R>(reader: R) -> Result<Vec<i32>, String> where R: BufRead {
    let mut coll = Vec::new();
    let mut num: i32;
    for line in reader.lines() {
        if let Ok(num_str) = line {
            num = num_str.parse::<i32>().map_err(|_| String::from("Parse error"))?;
            coll.push(num);
        }
    }
    Ok(coll)
}

fn find_triplet(coll: &Vec<i32>) -> Result<(i32, i32, i32), String> {

    for elem_1 in coll {
        for elem_2 in coll {
            for elem_3 in coll {
                if elem_1 + elem_2 + elem_3 == 2020 {
                    return Ok((*elem_1, *elem_2, *elem_3));
                }
            }
        }
    }
    Err("No valid triplet was found".to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_triplet_sample_input() {
        let coll = vec![1721, 979, 366, 299, 675, 1456];
        let result = find_triplet(&coll).unwrap();
        assert_eq!(result, (979, 366, 675));
    }
}
