/*
--- Day 3: Toboggan Trajectory ---
With the toboggan login problems resolved, you set off toward the airport. While travel by toboggan
might be easy, it's certainly not safe: there's very minimal steering and the area is covered in
trees. You'll need to see which angles will take you near the fewest trees.

Due to the local geology, trees in this area only grow on exact integer coordinates in a grid. You
make a map (your puzzle input) of the open squares (.) and trees (#) you can see. For example:

..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
These aren't the only trees, though; due to something you read about once involving arboreal
genetics and biome stability, the same pattern repeats to the right many times:

..##.........##.........##.........##.........##.........##.......  --->
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
You start on the open square (.) in the top-left corner and need to reach the bottom (below the
bottom-most row on your map).

The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers
rational numbers); start by counting all the trees you would encounter for the slope right 3, down 1:

From your starting position at the top-left, check the position that is right 3 and down 1. Then,
check the position that is right 3 and down 1 from there, and so on until you go past the bottom
of the map.

The locations you'd check in the above example are marked here with O where there was an open square
and X where there was a tree:

..##.........##.........##.........##.........##.........##.......  --->
#..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........X.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...#X....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
In this example, traversing the map using this slope would cause you to encounter 7 trees.

Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many
trees would you encounter?
*/


use std::io::{BufRead, Write};

#[derive(PartialEq, Debug)]
enum Square {
    Tree,
    Open,
}

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let map = parse_input(reader);
    let slope = Slope {
        right: 3,
        down: 1
    };
    let count = count_trees(&map, &slope);
    write!(&mut writer, "The number of trees encountered is: {}", count).unwrap();
}

fn parse_line(input_line: &str) -> Vec<Square> {
    let mut res = Vec::new();
    for c in input_line.chars() {
        match c {
            '.' => res.push(Square::Open),
            '#' => res.push(Square::Tree),
            _ => panic!("Invalid character")
        }
    }
    res
}

fn parse_input<R>(reader: R) -> Vec<Vec<Square>> where R: BufRead {
    let mut res = Vec::new();
    for line in reader.lines() {
        res.push(parse_line(&line.unwrap()));
    }
    res
}

fn count_trees(map: &Vec<Vec<Square>>, slope: &Slope) -> u32 {
    let mut count = 0;

    let mut line = 0;
    let mut column = 0;
    while line < map.len() {
        if let Square::Tree = &map[line][column % map[line].len()] {
            count += 1;
        }
        column += slope.right;
        line += slope.down;
    }
    count
}

struct Slope {
    down: usize,
    right: usize
}

#[cfg(test)]
mod tests {
    use super::*;
    use Square::*;

    #[test]
    fn test_parse_valid_line() {
        let input = "..##";
        assert_eq!(parse_line(input), vec![Open, Open, Tree, Tree]);
    }

    #[test]
    fn test_parse_input() {
        let input = b"..##\n#.#.";
        assert_eq!(parse_input(&input[..]), vec![vec![Open, Open, Tree, Tree],
                                                 vec![Tree, Open, Tree, Open]]);
    }

    #[test]
    fn test_count_trees() {
        let map = vec![vec![Open, Open, Tree, Tree],
                       vec![Tree, Tree, Tree, Open],
                       vec![Open, Tree, Open, Tree],
                       vec![Open, Tree, Tree, Tree],
        ];
        let slope = Slope { right: 1, down: 1};
        assert_eq!(count_trees(&map, &slope),  2);
    }

    #[test]
    fn test_solve() {
        let input = b"..##\n###.\n.#.#\n.###";
        let mut output = Vec::new();
        let expected = String::from("The number of trees encountered is: 1");

        solve(&input[..], &mut output);
        assert_eq!(String::from_utf8(output).unwrap(), expected);
    }
}
