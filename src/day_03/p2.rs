/*
--- Part Two ---
Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal stop, after all.

Determine the number of trees you would encounter if, for each of the following slopes, you start at the top-left corner and traverse the map all the way to the bottom:

- Right 1, down 1.
- Right 3, down 1. (This is the slope you already checked.)
- Right 5, down 1.
- Right 7, down 1.
- Right 1, down 2.
In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied together, these produce the answer 336.

What do you get if you multiply together the number of trees encountered on each of the listed slopes?
*/

use std::io::{BufRead, Write};

#[derive(PartialEq, Debug)]
enum Square {
    Tree,
    Open,
}

pub fn solve<R, W>(reader: R, mut writer: W) where R: BufRead, W: Write {
    let map = parse_input(reader);
    let slopes = vec![
        Slope { right: 1, down: 1 },
        Slope { right: 3, down: 1 },
        Slope { right: 5, down: 1 },
        Slope { right: 7, down: 1 },
        Slope { right: 1, down: 2 },
    ];
    let counts = slopes.iter().map(|s| count_trees(&map, &s));
    let result: u32 = counts.product();
    write!(&mut writer, "The product of the trees encountered is: {}", result).unwrap();
}

fn parse_line(input_line: &str) -> Vec<Square> {
    let mut res = Vec::new();
    for c in input_line.chars() {
        match c {
            '.' => res.push(Square::Open),
            '#' => res.push(Square::Tree),
            _ => panic!("Invalid character")
        }
    }
    res
}

fn parse_input<R>(reader: R) -> Vec<Vec<Square>> where R: BufRead {
    let mut res = Vec::new();
    for line in reader.lines() {
        res.push(parse_line(&line.unwrap()));
    }
    res
}

fn count_trees(map: &Vec<Vec<Square>>, slope: &Slope) -> u32 {
    let mut count = 0;

    let mut line = 0;
    let mut column = 0;
    while line < map.len() {
        if let Square::Tree = &map[line][column % map[line].len()] {
            count += 1;
        }
        column += slope.right;
        line += slope.down;
    }
    count
}

struct Slope {
    down: usize,
    right: usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve() {
        let input = b"..##\n###.\n.#.#\n.###";
        let mut output = Vec::new();
        let expected = String::from("The product of the trees encountered is: 1");

        solve(&input[..], &mut output);
        assert_eq!(String::from_utf8(output).unwrap(), expected);
    }
}
